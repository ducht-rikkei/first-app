import 'package:Tinder/UI/favorite_list.dart';
import 'package:Tinder/UI/home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:ui';

import 'Common/constants.dart';

void main() {
	runApp(MaterialApp(
		debugShowCheckedModeBanner: false,
		title: app_name,
		home: MyScaffold(),
	));
	SystemChrome.setEnabledSystemUIOverlays([]);
}

class MyScaffold extends StatefulWidget {
	@override
	MyScaffoldState createState() {
		return MyScaffoldState();
	}
}

class MyScaffoldState extends State<MyScaffold> {
	static bool currentPageIsHome = true;
	
	@override
	Widget build(BuildContext context) {
		return Material(
				child: Column(
						children: <Widget>[
							Container(
								height: 60.0,
								padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
								decoration: BoxDecoration(color: Colors.white),
								child: Row(
									children: <Widget>[
										IconButton(
											onPressed: () =>
													setState(() => currentPageIsHome = true),
											icon: Icon(
												Icons.home,
												color: Colors.black26,
											),
										),
										Expanded(
											child: Text(
												app_name,
												style: TextStyle(
													color: selectedColor,
													fontSize: 30,
													fontWeight: FontWeight.bold,
													fontFamily: 'Arial',
												),
												textAlign: TextAlign.center,
											),
										),
										IconButton(
											onPressed: () =>
													setState(() => currentPageIsHome = false),
											icon: Icon(Icons.favorite,
												color: Colors.black26,
											),
										),
									],
								),
							),
							buildPage()
						]
				)
		);
	}
	
	Widget buildPage() {
		if (currentPageIsHome) {
			return HomePage();
		} else
			return FavoriteList();
	}
}